import React, { useState, useEffect } from 'react';
import axios from 'axios';
import tw from "twin.macro";
import { css } from "styled-components/macro"; //eslint-disable-line
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import Hero from "components/hero/TwoColumnWithVideo.js";
import Features from "components/features/ThreeColSimple.js";
import MainFeature from "components/features/TwoColWithButton.js";
import MainFeature2 from "components/features/TwoColSingleFeatureWithStats2.js";
import TabGrid from "components/cards/TabCardGrid.js";
import Testimonial from "components/testimonials/ThreeColumnWithProfileImage.js";
import DownloadApp from "components/cta/DownloadApp.js";
import Footer from "components/footers/FiveColumnWithInputForm.js";

import chefIconImageSrc from "images/chef-icon.svg";
import celebrationIconImageSrc from "images/celebration-icon.svg";
import shopIconImageSrc from "images/shop-icon.svg";
import {PrimaryButton as PrimaryButtonBase} from "../components/misc/Buttons";

import styled from "styled-components";

export default () => {

    const [users, setUsers] = useState([]);
    const [recommend, setRecommend] = useState([]);
    const [related, setRelated] = useState([]);
    const [banner, setBanner] = useState([]);
    const [descBanner, setDescBanner] = useState([]);
    const [titleBanner, setTitleBanner] = useState([]);
    const [urlBanner, setUrlBanner] = useState([]);
    const [page, setPage] = useState(1);
    const [id, setId] = useState(Math.floor(Math.random() * 100) + 1);
    const [isLoading, setIsLoading] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    useEffect(() => {
        const loadUsers = async () => {
            try {
                setIsLoading(true);
                const response = await axios.get(
                    `https://cors-anywhere.herokuapp.com/https://api.jikan.moe/v4/anime/?page=${page}`
                    // `https://randomuser.me/api/?page=${page}&results=10`
                );

                setUsers((users) => [...users, ...response.data.data]);
                setErrorMsg('');
                console.log(users)
            } catch (error) {
                setErrorMsg('Error while loading data. Try again later.');
            } finally {
                setIsLoading(false);
            }
        }

        const loadBanner = async () => {
            try {
                setIsLoading(true);
                const response = await axios.get(
                    `https://api.jikan.moe/v4/anime/${id}`
                );

                setBanner(response.data.data.images.jpg.large_image_url);
                setTitleBanner(response.data.data.title);
                setDescBanner(response.data.data.synopsis);
                setUrlBanner(response.data.data.trailer?.embed_url)
                setErrorMsg('Not Found');
                console.log(id)
            } catch (error) {
                setErrorMsg('Error while loading data. Try again later.');
            } finally {
                setIsLoading(false);
            }
        };
        loadUsers();
        loadBanner();
        // loadUsers();
    }, [page]);

    const loadMore = () => {
        setPage((page) => page + 1);
    };

  const Subheading = tw.span`tracking-wider text-sm font-medium`;
  const HighlightedText = tw.span`bg-primary-500 text-gray-100 px-4 transform -skew-x-12 inline-block`;
  const HighlightedTextInverse = tw.span`bg-gray-100 text-primary-500 px-4 transform -skew-x-12 inline-block`;
  const Description = tw.span`inline-block mt-8`;
  const imageCss = tw`rounded-4xl`;
  const CardButton = tw(PrimaryButtonBase)`text-sm`;
    return (
    <AnimationRevealPage>
      <Hero
        heading={<>Watch <HighlightedText>{titleBanner}</HighlightedText></>}
        id = {id}
        watchVideoYoutubeUrl = {urlBanner}
        description={descBanner}
        imageSrc={banner}
        imageCss={imageCss}
        imageDecoratorBlob={true}
        primaryButtonText="Read More"
        watchVideoButtonText="Watch Trailer"
      />
      <TabGrid
          dataUsers={users}
          dataRecommend={recommend}
          dataRelated={related}
        heading={
          <>
            Checkout our <HighlightedText>collection.</HighlightedText>
          </>
        }
      />
        <div className="load-more" >
            <CardButton onClick={loadMore}>{isLoading ? 'Loading...' : 'Load More'}
            </CardButton>
        </div>
      <Features
        heading={
          <>
            Amazing <HighlightedText>Services.</HighlightedText>
          </>
        }
        cards={[
          {
            imageSrc: shopIconImageSrc,
            title: "230+ Locations",
            description: "Lorem ipsum donor amet siti ceali placeholder text",
            url: "https://google.com"
          },
          {
            imageSrc: chefIconImageSrc,
            title: "Professional Chefs",
            description: "Lorem ipsum donor amet siti ceali placeholder text",
            url: "https://timerse.com"
          },
          {
            imageSrc: celebrationIconImageSrc,
            title: "Birthday Catering",
            description: "Lorem ipsum donor amet siti ceali placeholder text",
            url: "https://reddit.com"
          }
        ]}

        imageContainerCss={tw`p-2!`}
        imageCss={tw`w-20! h-20!`}
      />
      <MainFeature2
        subheading={<Subheading>A Reputed Brand</Subheading>}
        heading={<>Why <HighlightedText>Choose Us ?</HighlightedText></>}
        statistics={[
          {
            key: "Orders",
            value: "94000+",
          },
          {
            key: "Customers",
            value: "11000+"
          },
          {
            key: "Chefs",
            value: "1500+"
          }
        ]}
        primaryButtonText="Order Now"
        primaryButtonUrl="https://order.now.com"
        imageInsideDiv={false}
        imageSrc="https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEzNzI2fQ&auto=format&fit=crop&w=768&q=80"
        imageCss={Object.assign(tw`bg-cover`, imageCss)}
        imageContainerCss={tw`md:w-1/2 h-auto`}
        imageDecoratorBlob={true}
        imageDecoratorBlobCss={tw`left-1/2 md:w-32 md:h-32 -translate-x-1/2 opacity-25`}
        textOnLeft={true}
      />
      {/*<Testimonial*/}
      {/*  subheading=""*/}
      {/*  heading={<>Customers <HighlightedText>Love Us.</HighlightedText></>}*/}
      {/*/>*/}
      <DownloadApp
        text={<>People around you are ordering delicious meals using the <HighlightedTextInverse>Treact App.</HighlightedTextInverse></>}
      />
      <Footer />
    </AnimationRevealPage>
  );
}
