import React, {useEffect, useState} from "react";
import tw from "twin.macro";
import { css } from "styled-components/macro"; //eslint-disable-line
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import Hero from "components/hero/TwoColumnWithInput.js";
import Features from "components/features/ThreeColWithSideImage.js";
import MainFeature from "components/features/TwoColWithButton.js";
import MainFeature2 from "components/features/TwoColWithTwoHorizontalFeaturesAndButton.js";
import FeatureWithSteps from "components/features/TwoColWithSteps.js";
import Pricing from "components/pricing/ThreePlans.js";
import Testimonial from "components/testimonials/TwoColumnWithImageAndRating.js";
import FAQ from "components/faqs/SingleCol.js";
import GetStarted from "components/cta/GetStarted";
import Footer from "components/footers/FiveColumnWithBackground.js";
import heroScreenshotImageSrc from "images/hero-screenshot-1.png";
import macHeroScreenshotImageSrc from "images/hero-screenshot-2.png";
import prototypeIllustrationImageSrc from "images/prototype-illustration.svg";
import { ReactComponent as BriefcaseIcon } from "feather-icons/dist/icons/briefcase.svg";
import { ReactComponent as MoneyIcon } from "feather-icons/dist/icons/dollar-sign.svg";
import Header from "../components/headers/light";
import axios from "axios";
import {useParams} from 'react-router-dom';
import TabGrid from "../components/cards/TabCardGrid";


export default ({ roundedHeaderButton = false, logoLink, links, className, collapseBreakpointClass = "lg" }) => {
  const Subheading = tw.span`uppercase tracking-widest font-bold text-primary-500`;
  const HighlightedText = tw.span`text-primary-500`;

  const [details, setDetails] = useState([]);
  const [more, setMore] = useState([]);
  const [errorMsg, setErrorMsg] = useState('');
  // const [id, setId] = useState(5);
  const {id} = useParams();

  console.log(id)

  useEffect(() => {
    const loadDetails = async () => {
      try {
        const response = await axios.get(
            `https://api.jikan.moe/v4/anime/${id}/full`
        );

        setDetails(response.data.data);
        setErrorMsg('');
        console.log(details)
      } catch (error) {
        setErrorMsg('Error while loading data. Try again later.');
      }
    }

    const loadMore = async () => {
      try {
        const response = await axios.get(
            `https://api.jikan.moe/v4/anime/${id}/recommendations`
        );

        setMore((more) => [...more, ...response.data.data]);
        setErrorMsg('');
        console.log(more)
      } catch (error) {
        setErrorMsg('Error while loading data. Try again later.');
      }
    }

    // loadMore();
    loadDetails();
  });

  return (
    <AnimationRevealPage>
      {/*<Hero roundedHeaderButton={true} />*/}
      <Header roundedHeaderButton={roundedHeaderButton} />
      <Testimonial
        subheading={<Subheading>Testimonials</Subheading>}
        heading={
          <>
            Our Clients <HighlightedText>Love Us.</HighlightedText>
          </>
        }
        testimonials={[
            details
        ]}
      />
      {/*<TabGrid*/}
      {/*    dataUsers={more}*/}
      {/*    heading={*/}
      {/*      <>*/}
      {/*        More Like <HighlightedText></HighlightedText>*/}
      {/*      </>*/}
      {/*    }*/}
      {/*/>*/}
    </AnimationRevealPage>
  );
}
